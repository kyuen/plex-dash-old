'use strict';

var gulp = require('gulp'),
	mocha = require('gulp-mocha'),
	jshint = require('gulp-jshint');
	
var jsFiles = ['lib/**/*.js', './*.js', 'test/**/**.js', 'public/js/**/*.js', 'routes/**/*.js'];
	
gulp.task('default', ['watch']);

gulp.task('jshint', function(){
	return gulp.src(jsFiles)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('watch', function(){
	gulp.watch(jsFiles, ['jshint', 'test']);
});

gulp.task('test', function(){
	return gulp.src('test/**/**.js', { read: false }).pipe(mocha());
});