'use strict';

const PlexAPI = require('plex-api');
const config = require('config');
const fs = require('fs');
const _ = require('lodash');

const client = new PlexAPI({
	hostname: config.hostname || 'localhost',
	port: config.port,
	username: config.username,
	password: config.password,
	token: config.token,
	identifier: 'c310b4aa-fb5c-47bf-8f32-ea6a633df517',
	product: 'Plex Dash',
	version: '0.0.1'
});

const plex = {
	getPlayingNow: function(useHardcoded) {
		if(useHardcoded) {
			return loadDataPromise.then(function (result) {
				return processResult(result);
			});
		}
		return client.query('/status/sessions').then(function(result) {
			return processResult(result);
		});
	},
};

function processResult(res) {
	let response = [];
	let mediaItems = res._children ? res._children : [];
	mediaItems.map(function(mediaItem) {
		mediaItem._children = _.keyBy(mediaItem._children, '_elementType');
		response.push({
			username: _.get(mediaItem, '_children.User.title'),
			displayTitle: getDisplayTitle(mediaItem),
			tvShowTitle: mediaItem.grandparentTitle,
			title: mediaItem.title,
			platform: _.get(mediaItem, '_children.Player.platform'),
			state: _.get(mediaItem, '_children.Player.state'),
			thumbnailUrl: 'http://' + (config.hostname || "localhost") + ':' + (config.port || "32400") + mediaItem.thumb,
			rating: mediaItem.rating,
			year: mediaItem.year,
			studio: mediaItem.studio,
			type: mediaItem.type,
			season: mediaItem.parentIndex,
			episode: mediaItem.index,
			time: Date.now(),
			duration: mediaItem.duration,
			viewOffset: mediaItem.viewOffset,
			viewCount: mediaItem.viewCount
		});
	});
	return response;
}

function getDisplayTitle(mediaItem) {
	return mediaItem.grandparentTitle
		? mediaItem.grandparentTitle + ': ' + mediaItem.title
		: mediaItem.title;
}

let loadDataPromise = new Promise(function (resolve, reject) {
	fs.readFile('./mockStatusSessionsResponse.json', 'utf8', function (err, data) {
		if (err) reject(err.message);
		resolve(JSON.parse(data));
	});
});

module.exports = plex;