'use strict';

const express = require('express');
const router = express.Router();
const plexProxy = require('../lib/plexProxy.js');

/* GET home page. */
router.get('/', (req, res) => {
	res.locals.title = 'Express';
	res.render('index');
});

router.get('/api/playingNow', (req, res) => {
	const useHardcoded = req.query.useHardcoded === "1";
	plexProxy.getPlayingNow(useHardcoded).then(function (result) {
		res.json(result);
	}).catch(err => { 
		res.status(500).end();
	});
});

module.exports = router;