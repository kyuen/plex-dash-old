'use strict';

var expect = require('chai').expect;
var proxyquire = require('proxyquire');

describe('plexProxy', function() {
	var plexProxy, configMock, plexApiMock, apiResponse;
	beforeEach(function() {
		configMock = { hostname: 'testhost', port: '555' };
		apiResponse = {};
		plexApiMock = function() {
			return {
				query: function() { return Promise.resolve(apiResponse); }
			};
		};
		plexProxy = proxyquire('../../../lib/plexProxy.js', {
			"config": configMock,
			"plex-api": plexApiMock
		});
	});
	describe('has a getPlayingNow() method', function() {
		describe('that returns an array', function() {
			it('containing the same number of objects as returned by the apiResponse', function() {
				apiResponse._children = [{ _elementType: 'Video' }, { _elementType: 'Video' }];
				return plexProxy.getPlayingNow().then(function(items) {
					expect(items).to.be.an('array');
					expect(items.length).to.equal(apiResponse._children.length);
				});
			});
			describe('containing objects', function() {
				it('with a username property from the apiResponse', function() {
					var expectedUsername = 'test-user';
					apiResponse._children = [{ _children: [{ _elementType: 'User', id: '1', thumb: 'http://test-thumb.com/thumbnail', title: expectedUsername }] }];
					plexProxy.getPlayingNow().then(function(items) {
						var mediaItem = items[0];
						expect(mediaItem.username).to.equal(expectedUsername);
					});
				});
				describe('with a displayTitle property', function() {
					it('from the title in the apiResponse when there is no grandparentTitle', function() {
						var expectedTitle = 'test movie';
						apiResponse._children = [{ _elementType: 'Video', title: expectedTitle }];
						plexProxy.getPlayingNow().then(function(items) {
							var mediaItem = items[0];
							expect(mediaItem.displayTitle).to.equal(expectedTitle);
						});
					});
					it('from the title and grandparentTitle in the apiResponse when grandparentTitle exists', function () {
						var tvShowTitle = 'Test TV Show';
						var episodeTitle = 'Test Episode';
						var expectedTitle = tvShowTitle + ': ' + episodeTitle;
						apiResponse._children = [{ _elementType: 'Video', title: episodeTitle, grandparentTitle: tvShowTitle }];
						plexProxy.getPlayingNow().then(function(items) {
							var mediaItem = items[0];
							expect(mediaItem.displayTitle).to.equal(expectedTitle);
						});
					});
				});
				it('with a platform and state property from the Player object on the apiResponse', function () {
					var expectedPlatform = 'test platform';
					var expectedState = 'paused';
					apiResponse._children = [{ _children: [{ _elementType: 'Player', platform: expectedPlatform, state: expectedState }] }];
					plexProxy.getPlayingNow().then(function (items) {
						var mediaItem = items[0];
						expect(mediaItem.platform).to.equal(expectedPlatform);
						expect(mediaItem.state).to.equal(expectedState);
					});
				});
				it('with a thumbnail url built from the apiResponse', function() {
					var thumbPath = '/library/metadata/1/thumb/2';
					var expectedUrl = 'http://' + configMock.hostname + ':' + configMock.port + thumbPath;
					apiResponse._children = [{ thumb: thumbPath }];
					plexProxy.getPlayingNow().then(function (items) {
						var mediaItem = items[0];
						expect(mediaItem.thumbnailUrl).to.equal(expectedUrl);
					});
				});
				it('with a rating property from the apiResponse', function () {
					testApiResponseConversion('rating', 'rating', 'testrating');
				});
				it('with a year property from the apiResponse', function () {
					testApiResponseConversion('year', 'year', '1923');
				});
				it('with a studio property from the apiResponse', function () {
					testApiResponseConversion('studio', 'studio', 'teststudio');
				});
				it('with a type property from the apiResponse', function () {
					testApiResponseConversion('type', 'type', 'movie');
				});
				it('with a season property from the apiResponse', function () {
					testApiResponseConversion('parentIndex', 'season', '3');
				});
				it('with a episode property from the apiResponse', function () {
					testApiResponseConversion('index', 'episode', '6');
				});
				it('with a duration property from the apiResponse', function () {
					testApiResponseConversion('duration', 'duration', '321893921');
				});
				it('with a viewOffset property from the apiResponse', function () {
					testApiResponseConversion('viewOffset', 'viewOffset', '3434');
				});
				it('with a viewCount property from the apiResponse', function () {
					testApiResponseConversion('viewCount', 'viewCount', '323432432432432');
				});
				it('with a tvShowTitle property from the apiResponse', function () {
					testApiResponseConversion('grandparentTitle', 'tvShowTitle', 'test tv show title');
				});
				it('with a title property from the apiResponse', function () {
					testApiResponseConversion('title', 'title', 'test title');
				});
				it('with a time property', function () {
					apiResponse._children = [{}];
					plexProxy.getPlayingNow().then(function (items) {
						var mediaItem = items[0];
						var date = new Date(mediaItem.time);
						var expectedDate = new Date(Date.now());
						expect(date.getUTCFullYear()).to.equal(expectedDate.getUTCFullYear());
						expect(date.getUTCMonth()).to.equal(expectedDate.getUTCMonth());
						expect(date.getUTCDay()).to.equal(expectedDate.getUTCDay());
					});
				});
			});
		});
		it('that returns an empty array if the response returns no children', function() {
			apiResponse._children = null;
			plexProxy.getPlayingNow().then(function(items) {
				expect(items).to.be.an('array');
				expect(items.length).to.equal(0);
			});
		});
	});
	function testApiResponseConversion(apiKey, responseKey, expectedValue) {
		var responseObj = {};
		responseObj[apiKey] = expectedValue;
		apiResponse._children = [ responseObj ];
		plexProxy.getPlayingNow().then(function (items) {
			var mediaItem = items[0];
			expect(mediaItem[responseKey]).to.equal(expectedValue);
		});
	}
});